#!/bin/bash

#install docker dependencies
[[ ! -e /.dockerenv ]] && exit 0

set -xe

#install git
apt-get update -yqq
apt-get install git mysql-client unzip -yqq

#install phpinit
curl --location --output /usr/local/bin/phpunit https://phar.phpunit.de/phpunit.phar
chmod +x /usr/local/bin/phpunit

#Install MySQL driver
#install other requirements here...

docker-php-ext-install pdo pdo_mysql

# Installs Sensiolabs security checker to check against unsecure libraries
php -r "readfile('http://get.sensiolabs.org/security-checker.phar');" > /usr/local/bin/security-checker
chmod +x /usr/local/bin/security-checker
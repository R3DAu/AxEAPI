# AxEAPI V0.2-Alpha
## Welcome to AxESIS's PHP Application Programming Interface [API]
As life has gone on, I've decided to build an API here using the latest in Agile and TDD recommendations. This current build is tested on 7, 7.1 and 7.2 versions of PHP and will be available to you for free here and is released under the GNU Public license V3.0.

I do not hold any responsibilities for this API whereby it causes data leaks nor the loss of data. I am a studying student and putting my many years of expertise in web-development here so you all can use this code for what you will without having to start from the beginning. I would presume that you will contact me through GitHub and if you have any issues that there are many other websites online that can help (I am fairly time-poor).

The directory structure will be mainly under vendor/axesis. You will find all the PHPUnit tests under vendor/axesis/UnitTest.


Kind regards,
R3DAu

# Requirements
## PHP 7.2 Setup
- PDO
- MySQL
- PHPSQLite3
- PHP_SQLite


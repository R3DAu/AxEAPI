<?php
require __DIR__ . '/vendor/autoload.php';
use \Firebase\JWT\JWT;
use \Zend\Config\Config;
use \Zend\Http\PhpEnvironment\Request;

$request = new Request();

if($request->isGet()){
  $authHeader = $request->getHeader('authorization');

  list($jwt) = sscanf( $authHeader->toString(), 'Authorization: Bearer %s');

  if($jwt){
    try{
      $config = new Config(include __DIR__ . '/config/conf.php');
      $secretKey = base64_decode($config->get('jwt')['key']);
      $token = JWT::decode($jwt, $secretKey, array($config->get('jwt')['algorithm']));
      header('Content-type: application/json');
      echo json_encode(array("success" => "OK"));
    }
    catch(Exception $e){
      header('HTTP/1.0 401 Unauthorized');
    }
  }
}else{
  header('HTTP/1.0 400 Bad Request');
}


?>

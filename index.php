<?php
/*
  AxEAPI - AxESIS PHP Application Programmable Interface [API]
  Copyright (C) 2019 Mitchell Reynolds & AxESIS

  This program is free software: you can redistribute it and/or modify
  it under the terms of the GNU General Public License as published by
  the Free Software Foundation, either version 3 of the License, or
  (at your option) any later version.

  This program is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU General Public License for more details.

  You should have received a copy of the GNU General Public License
  along with this program.  If not, see <https://www.gnu.org/licenses/>
*/

require __DIR__ . '/vendor/autoload.php';
require __DIR__ . '/vendor/axesis/autoloader.php';

use \AxESIS\Middleware\AuthTokens\AuthenticationTokens;
use \AxESIS\Core;

date_default_timezone_set("Australia/Melbourne");
$config = new \Zend\Config\Config(include __DIR__ . '\config\conf.php'); //Zend Config Object

//new authentication middleware
/*$at = new AuthenticationTokens();

if(empty($_GET['token'])){
  $at->generateToken('1', 'test');
}else{
  $at->decryptToken();
}*/
?>

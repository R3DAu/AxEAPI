<?php
/*
  AxEAPI - AxESIS PHP Application Programmable Interface [API]
  Copyright (C) 2019 Mitchell Reynolds & AxESIS

  This program is free software: you can redistribute it and/or modify
  it under the terms of the GNU General Public License as published by
  the Free Software Foundation, either version 3 of the License, or
  (at your option) any later version.

  This program is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU General Public License for more details.

  You should have received a copy of the GNU General Public License
  along with this program.  If not, see <https://www.gnu.org/licenses/>
*/

use AxESIS\Core;

//interface loads.
require __DIR__ . '/core/structs/endpoint.inc.php';

//load core components
require __DIR__ . '/core/moduleLoader.php';
require __DIR__ . '/core/endpointMapper.php';
require __DIR__ . '/core/router.php';

//collect the middleware
require __DIR__ . '/middleware/authTokenClass.php';
require __DIR__ . '/middleware/authentication.php';

//make sure we load the modules.
Core\moduleLoader::LoadModules();

//lets now load all the endpoints...
Core\endpointMapper::LoadEndpoints();

//lets now load the router...
Core\router::LoadRouter();

?>

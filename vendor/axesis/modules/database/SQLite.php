<?php
/*
  AxEAPI - AxESIS PHP Application Programmable Interface [API]
  Copyright (C) 2019 Mitchell Reynolds & AxESIS

  This program is free software: you can redistribute it and/or modify
  it under the terms of the GNU General Public License as published by
  the Free Software Foundation, either version 3 of the License, or
  (at your option) any later version.

  This program is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU General Public License for more details.

  You should have received a copy of the GNU General Public License
  along with this program.  If not, see <https://www.gnu.org/licenses/>
*/
namespace AxESIS\Modules;

use AxESIS\Modules;
use Zend\Config\Config;

require_once __DIR__ . '/Database.php';

/**
 *	SQLite Class
 *	This class is a wrapper for the SQLite database layer.
 */
class SQLite extends Database{
    public static $_config = array();
    private $pdo = null;

	public function __construct(bool $encrypt, string $dbConfig)
    {
        parent::__construct();
        self::$_config["encryption"]["enable"] = $encrypt;
        self::$_config["dbfile"] = $dbConfig;
        self::$_config["mainconf"] = false;
    }

    /**
     *	Connect function
     *	This function makes a PDO connection and/or initializes the database file.
     *
     *	Param: Nill
     *	Returns: PDO Object, an initilized PDO Object.
     */
    public function Connect() : \PDO
    {
        parent::Connect();

        //let's try to connect to this db now
        try{
            if($this->checkDatabaseFileExists(self::$_config["dbfile"]))
                $this->pdo = new \PDO("sqlite:" . self::$_config["dbfile"]);
            else
            {
                $sql = file_get_contents(realpath(__DIR__ . '/../../dist/sql') . '/sqlite.sql');
                $this->pdo = new \PDO("sqlite:" . self::$_config["dbfile"]);
                $this->pdo->exec($sql);

                if(self::$_config['encryption']["enable"]){
                    //insert the new api admin user.
                    $stmt = $this->pdo->prepare("INSERT INTO api_users (user_name, password, group_id) VALUES (:un, :pw, 1)");
                    $stmt->bindValue(":un", Modules\crypt::Encrypt("admin"));
                    $stmt->bindValue(":pw", Modules\crypt::Encrypt(password_hash("password", PASSWORD_DEFAULT)));
                    $stmt->execute();
                }
                else
                {
                    //insert the new api admin user.
                    $stmt = $this->pdo->prepare("INSERT INTO api_users (user_name, password, group_id) VALUES (:un, :pw, 1)");
                    $stmt->bindValue(":un", "admin");
                    $stmt->bindValue(":pw",password_hash("password", PASSWORD_DEFAULT));
                    $stmt->execute();
                }
            }
        }catch(\PDOException $e){
            throw new Exception("Null PDO Exception, Check file permissions.", 500, $e);
        }

        return $this->pdo;
    }

    /**
     *	Get function
     *	This function returns an assoc array of data from the specified table and parameters.
     *
     *	Param: $table, string - the table name
     *  Param: $fields, string - the selected fields
     *  Param: $where, string - the where filter.
     *	Returns: Array, Associative array of objects.
     */
    public function Get(string $table, $fields = "*", string $where = NULL)
    {
	    $table = filter_var($table, FILTER_SANITIZE_STRING);
	    $fields= filter_var($fields, FILTER_SANITIZE_STRING);
	    $where=($where!==NULL)?filter_var($where, FILTER_SANITIZE_STRING):NULL;

	    $sql = (!empty($where)) ? "SELECT $fields FROM $table WHERE $where" : "SELECT $fields FROM $table";
	    return $this->pdo->query($sql)->fetchAll(\PDO::FETCH_ASSOC);
    }

    //check if the file
    private function checkDatabaseFileExists(string $file) : bool
    {
        if(file_exists(filter_var($file, FILTER_SANITIZE_STRING)))
            return true;
        else
            return false;
    }
}


?>

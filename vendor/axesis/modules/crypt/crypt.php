<?php
/*
  AxEAPI - AxESIS PHP Application Programmable Interface [API]
  Copyright (C) 2019 Mitchell Reynolds & AxESIS

  This program is free software: you can redistribute it and/or modify
  it under the terms of the GNU General Public License as published by
  the Free Software Foundation, either version 3 of the License, or
  (at your option) any later version.

  This program is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU General Public License for more details.

  You should have received a copy of the GNU General Public License
  along with this program.  If not, see <https://www.gnu.org/licenses/>
*/
namespace AxESIS\Modules;

use Zend\Config\Config;
use Zend\Crypt\BlockCipher;


/**
 *	Crypt Class
 *	This class allows for on-the-fly encryption.
 */
class crypt
{
    /**
     *	Encrypt Function
     *	This function encrypts a message using Zend's BlockCipher.
     *
     *	Param: $message, the message to encrypt
     *	Returns: string, the encrypted message
     */
    public static function Encrypt($message) : string
    {
        $config = new Config(include realpath(__DIR__ . '\..\..\..\..\config\dbConf.php'));

        $blockCipher = BlockCipher::factory(
            'openssl',
            [
                'algo' => $config['encryption']['algo'],
                'mode' => $config['encryption']['mode']
            ]
        );

        $blockCipher->setKey($config['encryption']['key']);
        return base64_encode($blockCipher->encrypt($message));
    }

    /**
     *	Decrypt Function
     *	This function decrypts a message using Zend's BlockCipher.
     *
     *	Param: $message, the message to decrypt
     *	Returns: string, the decrypted message
     */
    public static function Decrypt($message) : string
    {
        $config = new Config(include realpath(__DIR__ . '\..\..\..\..\config\dbConf.php'));

        $blockCipher = BlockCipher::factory(
            'openssl',
            [
                'algo' => $config['encryption']['algo'],
                'mode' => $config['encryption']['mode']
            ]
        );

        $blockCipher->setKey($config['encryption']['key']);
        return $blockCipher->decrypt(base64_decode($message));
    }

    /**
     *	Generate Encryption Key
     *	This function generates an encryption key for the config file.
     *
     *	Param: Nil
     *	Returns: string, The hexadecimal string value
     */
    public static function GenerateEncryptionKey() : string
    {
        return bin2hex(random_bytes(32));
    }
}


?>

CREATE TABLE IF NOT EXISTS user_groups(
    group_id INTEGER PRIMARY KEY,
    level INTEGER NOT NULL,
    name TEXT NOT NULL
);

INSERT INTO user_groups VALUES (1, 50, "Super Administrator");
INSERT INTO user_groups VALUES (2, 40, "Administrator");
INSERT INTO user_groups VALUES (3, 30, "Moderator");
INSERT INTO user_groups VALUES (4, 20, "Registered User");
INSERT INTO user_groups VALUES (5, 10, "Moderated User");
INSERT INTO user_groups VALUES (6, 1, "Guest");
INSERT INTO user_groups VALUES (7, 0, "Restricted User");

CREATE TABLE IF NOT EXISTS api_users(
    user_id INTEGER PRIMARY KEY,
    user_name TEXT NOT NULL,
    password TEXT NOT NULL,
    group_id INTEGER NOT NULL DEFAULT 6,
    FOREIGN KEY(group_id) REFERENCES user_groups(group_id)
);

CREATE TABLE IF NOT EXISTS api_settings(
    setting TEXT UNIQUE PRIMARY KEY,
    value TEXT NULL
);
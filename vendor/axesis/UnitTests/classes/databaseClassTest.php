<?php
namespace DatabaseTest\UnitTests;

require __DIR__ . '/databaseTestCase.php';

class UserMapperTest extends databaseTestCase
{
  public function getDataSet()
  {
     return $this->createFlatXMLDataSet( __DIR__ . '/user-test.xml' );
  }

  public function testFetchAll()
  {
    $result = array(self::$pdo->query("SELECT user_id,username,salt FROM `users` LIMIT 1")->fetch(\PDO::FETCH_ASSOC));
    $this->assertEquals(array(array("user_id" => "1", "username"=>"test", "salt" => "salty")), $result);
  }

}



?>

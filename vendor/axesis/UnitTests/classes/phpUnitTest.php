<?php
class PhpUnitTesterTest extends PHPUnit\Framework\TestCase
{
  /**
  * @dataProvider additionProvider
  */
  public function testAssert($a, $b, $expected)
  {
    $this->assertEquals($expected, $a+$b);
  }

  public function additionProvider()
  {
    return [
      'zero' => [0,0,0],
      'ones' => [1,1,2],
      'two'  => [2,2,4]
    ];
  }
}
?>

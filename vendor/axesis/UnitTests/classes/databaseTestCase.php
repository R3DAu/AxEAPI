<?php
namespace DatabaseTest\UnitTests;

abstract class databaseTestCase extends \PHPUnit\DbUnit\TestCase
{
  static protected $pdo = null;

  final public function getConnection()
  {
      if(self::$pdo == null)
        self::$pdo = new \PDO( $GLOBALS['DB_DSN'], $GLOBALS['DB_USER'], $GLOBALS['DB_PASSWD'] );

     return $this->createDefaultDBConnection(self::$pdo, $GLOBALS['DB_DBNAME']);
  }
}
?>

<?php
/*
  AxEAPI - AxESIS PHP Application Programmable Interface [API]
  Copyright (C) 2019 Mitchell Reynolds & AxESIS

  This program is free software: you can redistribute it and/or modify
  it under the terms of the GNU General Public License as published by
  the Free Software Foundation, either version 3 of the License, or
  (at your option) any later version.

  This program is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU General Public License for more details.

  You should have received a copy of the GNU General Public License
  along with this program.  If not, see <https://www.gnu.org/licenses/>
*/
namespace AxESIS\Middleware;
use \Firebase\JWT\JWT;
use \Zend\Config\Config;
use \Zend\Http\PhpEnvironment\Request;

class AuthenticationTokens{
  public static $TokenData = null;

  //this will generate the secret key for JWT config
  function generateSecret(){
    return base64_encode(openssl_random_pseudo_bytes(64));
  }

  //this generates the JWT token.
  public static function generateToken($userId, $userName, $groupId){
    $config = new Config(include __DIR__ . '\..\..\..\config\conf.php'); //Zend Config Object

    $tokenId   = base64_encode(mcrypt_create_iv(32));
    $issuedAt  = time();
    $notBefore = $issuedAt + 10;  //add 10 seconds
    $expire    = $notBefore + 60; //add 60 seconds
    $serverName= $config->get('serverName');

    $data = [
      'iat'  => $issuedAt,
      'jti'  => $tokenId,
      'iss'  => $serverName,
      'nbf'  => $notBefore,
      'exp'  => $expire,
      'data' => [
        'userId'   => filter_var($userId, FILTER_SANITIZE_NUMBER_INT),
        'userName' => filter_var($userName, FILTER_SANITIZE_STRING),
        'groupId'  => filter_var($groupId, FILTER_SANITIZE_NUMBER_INT)
      ]
    ];

    //base64_encode(openssl_random_pseudo_bytes(64)); Generated
    $secretKey = base64_decode($config->get('jwt')['key']);

    $jwt = JWT::encode(
      $data,
      $secretKey,
      $config->get('jwt')['algorithm']
    );

    return $jwt;
  }

  //decrypts the authroization header
  public static function decryptToken(){
    $request = new Request();
    $config = new Config(include __DIR__ . '\..\..\..\config\conf.php'); //Zend Config Object
    $authHeader = $request->getHeader('authorization');
    list($jwt) = sscanf( $authHeader->toString(), 'Authorization: Bearer %s');

    if($jwt){
      try{
        $secretKey = base64_decode($config->get('jwt')['key']);
        $token = JWT::decode($jwt, $secretKey, array($config->get('jwt')['algorithm']));
        self::$TokenData = $token;
        return true;
      }
      catch(\Exception $e){
        header('HTTP/1.0 401 Unauthorized');
        return false;
      }
    }
  }
}



?>

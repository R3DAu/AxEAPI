<?php
/*
  AxEAPI - AxESIS PHP Application Programmable Interface [API]
  Copyright (C) 2019 Mitchell Reynolds & AxESIS

  This program is free software: you can redistribute it and/or modify
  it under the terms of the GNU General Public License as published by
  the Free Software Foundation, either version 3 of the License, or
  (at your option) any later version.

  This program is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU General Public License for more details.

  You should have received a copy of the GNU General Public License
  along with this program.  If not, see <https://www.gnu.org/licenses/>
*/
namespace AxESIS\Middleware;

use AxESIS\Modules;
use AxESIS\Middleware;
use Zend\Config\Config;

/**
 *	Authentication Class
 *	This class authenticates API users.
 */

class Authentication
{
    /**
     *	Authenticate API User Function
     *	This function authenticates an API user and sends back a JWT token if successful.
     *
     *	Param: Nill
     *	Returns: JWT Token if successful, false if not.
     */
    public static function AuthenticateAPIUser(string $username, string $password)
    {
        //make sure the username and password given is scrubbed first.
        $u = filter_var($username, FILTER_SANITIZE_STRING);
        $p = filter_var($password, FILTER_SANITIZE_STRING);

        //now make sure they aren't empty values...
        if(empty($username) || empty($password)) throw new \Exception("Username or Password cannot be empty!", 403);

        //collect the database configurations.
        $config = new Config(include realpath(__DIR__ . '\..\..\..\config\conf.php'));
        $sqldb = new Modules\SQLite(true, $config['configDB']);

        //make sure we turn this on to tell other scripts this is the main configuration database.
        $sqldb::$_config["mainconf"] = true;

        //now send the connect to initilize the database.
        $sqldb->Connect();

        //we only need to do this process if the whole table is encrypted.
        if($sqldb::$_config["encryption"]["enable"] == true)
        {
            //now select all users in the api_users table.
            $users = $sqldb->Get("api_users");
            //cycle through all the users.
            foreach($users as $user)
            {
                if($username == Modules\crypt::Decrypt($user["user_name"]))
                    if(password_verify($password, Modules\crypt::Decrypt($user["password"])))
                       return json_encode(array("token" => Middleware\AuthenticationTokens::generateToken($user["user_id"], $user["user_name"], $user["group_id"]), "success" => true));
            }
        }
        else
        {
            //unencrypted database.
            $users = $sqldb->Get("api_users", "*", "user_name=$username");
            if(count($users) <= 0) json_encode(array("success" => false));
            elseif(password_verify($password,$users[0]["password"]))  return json_encode(array("token" => Middleware\AuthenticationTokens::generateToken($users[0]["user_id"], $users[0]["user_name"], $users[0]["group_id"]), "success" => true));
        }

        return json_encode(array("success" => false));
    }
}
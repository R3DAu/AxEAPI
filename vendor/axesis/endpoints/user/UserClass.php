<?php
/*
  AxEAPI - AxESIS PHP Application Programmable Interface [API]
  Copyright (C) 2019 Mitchell Reynolds & AxESIS

  This program is free software: you can redistribute it and/or modify
  it under the terms of the GNU General Public License as published by
  the Free Software Foundation, either version 3 of the License, or
  (at your option) any later version.

  This program is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU General Public License for more details.

  You should have received a copy of the GNU General Public License
  along with this program.  If not, see <https://www.gnu.org/licenses/>
*/
namespace AxESIS\Endpoints;

/**
*	Module Loader Class
*	This class enables the automatic process of registering modules
*	for the API. This system will be used directly by the router
*	inbuilt into the API to route to specific modules etc.
*/
class UserClass implements iEndpoint 
{
	function Post(array $data){}
	function Get(array $data)
    {
        print_r($data);
    }
	function Delete(array $data){}
	function Put(array $data){}
}
<?php
/*
  AxEAPI - AxESIS PHP Application Programmable Interface [API]
  Copyright (C) 2019 Mitchell Reynolds & AxESIS

  This program is free software: you can redistribute it and/or modify
  it under the terms of the GNU General Public License as published by
  the Free Software Foundation, either version 3 of the License, or
  (at your option) any later version.

  This program is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU General Public License for more details.

  You should have received a copy of the GNU General Public License
  along with this program.  If not, see <https://www.gnu.org/licenses/>
*/
namespace AxESIS\Core;

use AxESIS\Modules;
use AxESIS\Middleware;
use Zend\Config\Config;

/**
 *	Router Class
 *	This class allows the interaction between endpoints, data and everything else.
 */

class router
{
    public static $Endpoint = null;
    public static $RequestMethod = null;
    public static $Query = null;
    public static $Token = null;
    public static $EndpointClass = null;

    /**
     *	Load Router function
     *	This function works out all the endpoint mappings and where the requests should go.
     *
     *	Param: Nill
     *	Returns: Void
     */
    public static function LoadRouter()
    {
        //we will set our headers here...
        header('Content-type: application/json');

        //let's get the request method. Convert the string into proper camelcase
        self::$RequestMethod = ucfirst(strtolower(filter_var($_SERVER['REQUEST_METHOD'], FILTER_SANITIZE_STRING)));
        //let's check the request.
        if(!self::CheckMethod(self::$RequestMethod)){
            header('HTTP/1.0 405 Method Not Allowed');
            throw new \Exception("Method Not Allowed.", 405);
        }

        //let's load the endpoint...
        $endpoint = (filter_var(trim($_GET['r'], '/'), FILTER_SANITIZE_STRING));

        //we also need to authenticate the using the middleware modules...
        if($endpoint == "auth")
        {
            if($_SERVER['REQUEST_METHOD'] !== 'POST') throw new \Exception("Method Not Allowed", 405);
            print_r(Middleware\Authentication::AuthenticateAPIUser(filter_var($_POST['username'],FILTER_SANITIZE_STRING), filter_var($_POST['password'], FILTER_SANITIZE_STRING)));
            return;
        }

        //make sure our token is authentic
        if(Middleware\AuthenticationTokens::decryptToken() === false)
        {
            throw new \Exception("Unauthorized", 401);
        }

        //now we need to check if the endpoint exists...
        try
        {
            self::$EndpointClass = endpointMapper::GetEndpointClass($endpoint);
        }
        catch(\Exception $e)
        {
            throw new \Exception("Endpoint not found.", 404);
        }

        //now lets pass to collecting the data needed...
        switch(self::$RequestMethod)
        {
            case "Get":
                self::$Query = @filter_var_array($_GET, FILTER_SANITIZE_STRING);
                unset(self::$Query['r']);
                break;
            case "Post":
            case "Put":
            case "Delete":
                self::$Query = json_decode(file_get_contents("php://input"),true);
        }
        //prepare for the response and send headers now...
        header("Access-Control-Allow-Origin: *");
        header("Content-Type: application/json; charset=UTF-8");

        //convert to string.
        $m = self::$RequestMethod;

        //if we got this far then the endpoint exsits and now we can try to launch the requested accepted request method
        self::$EndpointClass->$m(array(self::$Query));
    }

    /**
     *	Check Method Function
     *	This function checks if the method is allowed (following CRUD)
     *
     *	Param: Nill
     *	Returns: Void
     */
    private static function CheckMethod(string $method) : bool
    {
        //CRUD (Create, Read, Update, Delete) -> In REST Notation (Post, Get, Put, Delete).
        switch(filter_var($method, FILTER_SANITIZE_STRING))
        {
            case "Post":
            case "Get":
            case "Put":
            case "Delete":
                return TRUE;
                break;
            default:
                return FALSE;
        }
    }
}
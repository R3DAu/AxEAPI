<?php
/*
  AxEAPI - AxESIS PHP Application Programmable Interface [API]
  Copyright (C) 2019 Mitchell Reynolds & AxESIS

  This program is free software: you can redistribute it and/or modify
  it under the terms of the GNU General Public License as published by
  the Free Software Foundation, either version 3 of the License, or
  (at your option) any later version.

  This program is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU General Public License for more details.

  You should have received a copy of the GNU General Public License
  along with this program.  If not, see <https://www.gnu.org/licenses/>
*/
namespace AxESIS\Core;

/**
*	Module Loader Class
*	This class enables the automatic process of registering modules
*	for the API. This system will be used directly by the router
*	inbuilt into the API to route to specific modules etc.
*/
class moduleLoader
{
	/**
	*	Modules Array
	*	This variable will store all the information from
	*	the Register Module function. Later on this manual way
	*	of adapting the data will be replaced with a database 
	*   varient of storing module information properly. 
	*/
	public static $Modules = array();
	
	
	/**
	*	Load Modules
	*	This function automatically load all the modules
	*	registered in the Modules array. 
	*	
	*	Param: Nill
	*	Returns: Void
	*/
	public static function LoadModules()
	{
		//check if the registermodules function has been ran
		if(count(self::$Modules) <= 0)
			self::RegisterModules(); //run the registration module if it wasn't ran.
		
		//collect each module and prepare to load them with the paths defined.
		foreach(self::$Modules as $key => $module)
		{
			//check if the json is okay.
			if(array_key_exists("LoadFiles", $module) && array_key_exists("Path", $module))
			{
				foreach($module['LoadFiles'] as $file)
					require $module['Path'] . '\\' . $file;
			}
			
			//tell the appliaction that the script is loaded. 
			self::$Modules[$key]["Loaded"] = true;
		}
		
		//tell the appliaction that the script is loaded. 
		self::$Modules[$key]["Loaded"] = true;
		
		//now check the dependancies.
		//collect each module and prepare to load them with the paths defined.
		foreach(self::$Modules as $key => $module)
		{
			//check if the json is okay.
			if(array_key_exists("Dependancies", $module))
			{
				if(count($module['Dependancies']) >= 1)
					foreach($module['Dependancies'] as $dep)
					{
						$mod = explode(":", $dep);
						if(!self::CheckModuleLoaded($mod[0]))
						{
							//reset the state.
							self::$Modules[$key]['Loaded'] = false;
							$j = json_decode(self::$Modules[$key]['State'], true);
							$j[] = "Dependancy for $key was not found: $dep";
							self::$Modules[$key]['State'] = $j;
						}			
					}
			}
		}
	}
	
	/**
	*	Check Module Loaded
	*	This function automatically load all the modules
	*	registered in the Modules array. 
	*	
	*	Param: $ModuleName, Name of the module
	*	Returns: Bool, Whether the module is loaded. 
	*/
	public static function CheckModuleLoaded($moduleName)
	{
		if(count(self::$Modules) <= 0)
			return false; // no modules have been loaded
		
		if(array_key_exists($moduleName, self::$Modules))
			return self::$Modules[$moduleName]['Loaded'];
		else
			return false; //the module doesn't exist.
	}
	
	
	/**
	*	Register Modules
	*	This function automatically checks and
	*	registers functions. It will then put the 
	*	registered modules in the $modules array.
	*	
	*	Param: Nill
	*	Returns: Void
	*/
	public static function RegisterModules()
	{
		$jsonFiles = self::scanDir(__DIR__ . '/../modules/');
		
		foreach($jsonFiles as $jsonFile)
		{
			//get the json array and now setup the modules array
			$ja = self::jsonFileParser($jsonFile);
			
			//Format the output to suit the module.
			self::$Modules[$ja['Module']] = array(
				'Version'      => $ja['Version'],
				'Dependancies' => $ja['Dependancies'],
				'LoadFiles'    => $ja['Load'],
				'Endpoints'    => $ja['Endpoints'],
				'Path'         => $ja['ModulePath'],
				'CoreComponent'=> $ja['Core Component'],
				'Loaded'	   => false,
				'State'		   => "{}"
			);
		}
	}
	
	/**
	*	JSON File Parser
	*	Converts a JSON File to a JSON PHP Array
	*	
	*	Param:
	*		- $target = string, used to collect the target file
	*	Returns: 
	*		- $json = array, The JSON Array
	*/
	private static function jsonFileParser($target)
	{
		//make sure the JSON file exists.
		if(!file_exists($target)) die('JSON File doesn\'t exist');
		
		$jsonFile = file_get_contents($target);
		$json = json_decode($jsonFile, true);
		$json['ModulePath'] = realpath(dirname($target));
		
		return $json;
	}
	
	/**
	*	Scan Directory
	*	This function recursively scans a directory and will include any 
	*   module json files.
	*	
	*	Param:
	*		- $target = string, used to collect the target directory
	*	Returns: 
	*		- $fileArray = array, an array of files matching the file extension.
	*/
	private static function scanDir($target)
	{
		$fileArray = array();
		//check if the target is a directory or a file.
		if(is_dir($target))
		{
			//we have to now check the target directory for more folders...
			$files = glob($target . '*', GLOB_MARK); //this adds a slash to the end of all the directories...
			
			foreach($files as $file)
				foreach(self::scanDir($file) as $f)
					$fileArray[] = $f;
		}
		else
		{
			//check the extension of the target.
			$ext = pathinfo($target, PATHINFO_EXTENSION);
			if($ext == "json")
				$fileArray[] = $target;
		}
		
		return $fileArray;
	}
}
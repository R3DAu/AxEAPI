<?php
/*
  AxEAPI - AxESIS PHP Application Programmable Interface [API]
  Copyright (C) 2019 Mitchell Reynolds & AxESIS

  This program is free software: you can redistribute it and/or modify
  it under the terms of the GNU General Public License as published by
  the Free Software Foundation, either version 3 of the License, or
  (at your option) any later version.

  This program is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU General Public License for more details.

  You should have received a copy of the GNU General Public License
  along with this program.  If not, see <https://www.gnu.org/licenses/>
*/
namespace AxESIS\Core;

use AxESIS\Endpoints\iEndpoint;
use Zend\Config\Config;
use AxESIS\Endpoints;

/**
*	Endpoint Mapper Class
*	This class keeps track of endpoints and maps them to a module class.
*	for the API. This system will be used directly by the router
*	inbuilt into the API to route to specific modules etc.
*/
class endpointMapper
{
    /**
     *	Endpoints Array
     *	This variable will store all the information from
     *	the Load Endpoints function. Later on this manual way
     *	of adapting the data will be replaced with a database
     *   varient of storing module information properly.
     */
    public static $Endpoints = array();

    /**
     *	Load Endpoints
     *	This function automatically loads all the endpoints
     *	registered in the Modules array.
     *
     *  Note: Every module can have multiple endpoints. For example
     *  a user module might have /users/ but it might also handle
     *  /auth/ and /user/ endpoints too.
     *
     *	Param: Nill
     *	Returns: Void
     */
	public static function LoadEndpoints()
    {
        $config = new Config(include realpath(__DIR__ . '\..\..\..\config\conf.php')); //Zend Config Object

        //collect the modules and cycle through to find the endpoint list.
        foreach(moduleLoader::$Modules as $module)
        {
            if(array_key_exists("Endpoints", $module) && is_array($module['Endpoints']))
            {
                foreach($module['Endpoints'] as $endpoint)
                {
                    //explode the string
                    $raw = explode(":", $endpoint);
                    $ep = $raw[0];
                    $class = $raw[1];

                    //remap the endpoints with the proper slashes
                    $default_path = glob($config["endPointDir"] . '*', GLOB_MARK)[0];
                    //markout the correct php files.
                    $file =  glob($default_path . filter_var($ep, FILTER_SANITIZE_STRING) . '*', GLOB_MARK)[0] . filter_var($class, FILTER_SANITIZE_STRING) . ".php";

                    //make sure the file already exists.
                    if(file_exists($file))
                        include_once $file; //import the file into the build.

                    //check the for the current class
                    $vclass = "\\AxESIS\\Endpoints\\" . $class;

                    //make sure we have the class.
                    if(class_exists($vclass))
                       self::$Endpoints[] = array("endPoint" => $ep, "Class" => new $vclass()); //load the endpoint class into the array.
                }
            }
        }
    }

     /**
     *	Get Endpoint Class
     *	This helper function returns the class of the endpoint
     *	registered in the Endpoints array.
     *
     *	Param: $endpoint, This is the endpoint to search for
     *	Returns: iEndpoint interface class, any endpoints with the correctly registered with the iEndpoint class
     */
    public static function GetEndpointClass(string $endpoint) : iEndpoint
    {
        //do a multidemensional array search (in the endPoint Column) for the endpoint string.
        $result = array_search(filter_var($endpoint, FILTER_SANITIZE_STRING), array_column(self::$Endpoints, 'endPoint'));
        //if the search returns a negative then throw an exception. On the otherside we should have a try-catch block to handle the exception
        if($result === FALSE) throw new \Exception("Unable to get endpoint. Possible reason is that the endpoint doesn't exist?");

        return self::$Endpoints[$result]["Class"];
    }
}